# Barcelona
---
Text amb informació de Barcelona

## Llista ordenada de barris
1. Noubarris
2. Guinardo
3. Trinitat

## Llista desordenada de barris
- Trinitat Nova
- Guineueta
- Ciutat vella

## Enllaç a una web [enllaç a Barcelona](https://www.barcelona.cat/ca/)

# Barcelona
![Imatge de Barcelona](images/barcelona.jpg)
